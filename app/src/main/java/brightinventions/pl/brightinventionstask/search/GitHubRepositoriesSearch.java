package brightinventions.pl.brightinventionstask.search;

import android.text.Html;

import org.springframework.web.client.RestOperations;

import brightinventions.pl.brightinventionstask.search.model.GitHubSearchResult;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class GitHubRepositoriesSearch {
	private static final String urlFormat = "https://api.github.com/search/repositories?q=%s&sort=stars&order=desc";

	private final RestOperations restOperations;

	public GitHubSearchResult search(String query) {
		String url = String.format(urlFormat, Html.escapeHtml(query));
		return restOperations.getForObject(url, GitHubSearchResult.class);
	}
}
