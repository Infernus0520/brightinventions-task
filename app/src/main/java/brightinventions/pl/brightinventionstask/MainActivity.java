package brightinventions.pl.brightinventionstask;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import brightinventions.pl.brightinventionstask.search.GitHubRepositoriesSearch;
import brightinventions.pl.brightinventionstask.search.model.GitHubSearchResult;

public class MainActivity extends AppCompatActivity {
	private static final String TAG = "MainActivity";

	// substitute for DI
	private final RestTemplate restTemplate = new RestTemplate();
	{
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
	}
	private final GitHubRepositoriesSearch gitHubRepositoriesSearch = new GitHubRepositoriesSearch(restTemplate);

	private ItemsAdapter itemsAdapter;

	private EditText searchInput;
	private SwipeRefreshLayout swipeContainer;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		itemsAdapter = new ItemsAdapter(this, new ArrayList<GitHubSearchResult.Item>());

		ListView listView = (ListView) findViewById(R.id.searchResultsView);
		listView.setAdapter(itemsAdapter);

		searchInput = (EditText) findViewById(R.id.searchInput);
		searchInput.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}

			@Override
			public void afterTextChanged(Editable s) {
				invokeSearch(s.toString());
			}
		});

		swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				invokeSearch(searchInput.getText().toString());
			}
		});
	}

	private void invokeSearch(String query) {
		if (query != null && !"".equals(query)) {
			new SearchTask().execute(query);
		} else {
			itemsAdapter.clear();
		}
	}

	private class SearchTask extends AsyncTask<String, Void, GitHubSearchResult> {
		@Override
		protected GitHubSearchResult doInBackground(String... params) {
			Log.i(TAG, params[0]);
			try {
				return gitHubRepositoriesSearch.search(params[0]);
			} catch (Exception e) {
				Log.e(TAG, e.getMessage(), e);
				Toast.makeText(MainActivity.this, "An error occurred while search.", Toast.LENGTH_SHORT).show();
				return null;
			}
		}

		@Override
		protected void onPostExecute(GitHubSearchResult searchResult) {
			itemsAdapter.clear();
			itemsAdapter.addAll(searchResult.getItems());

			swipeContainer.setRefreshing(false);

			Log.i(TAG, String.valueOf(searchResult));
		}
	}

	private class ItemsAdapter extends ArrayAdapter<GitHubSearchResult.Item> {
		public ItemsAdapter(Context context, List<GitHubSearchResult.Item> items) {
			super(context, 0, items);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// Get the data item for this position
			GitHubSearchResult.Item item = getItem(position);

			// Check if an existing view is being reused, otherwise inflate the view
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.search_result_item, parent, false);
			}
			
			// Lookup view for data population
			TextView name = (TextView) convertView.findViewById(R.id.name);
			TextView starCount = (TextView) convertView.findViewById(R.id.starCount);
			ImageView avatar = (ImageView) convertView.findViewById(R.id.avatar);

			// Populate the data into the template view using the data object
			name.setText(item.getName());
			starCount.setText(Integer.toString(item.getStargazersCount()));
			if (item.getOwner().getAvatarUrl() != null) {
				new DownloadImageTask(avatar).execute(item.getOwner().getAvatarUrl());
			}

			// Return the completed view to render on screen
			return convertView;
		}
	}

	private class DownloadImageTask extends AsyncTask<URL, Void, Bitmap> {
		private final ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		@Override
		protected Bitmap doInBackground(URL... imageUrls) {
			Log.i(TAG, imageUrls[0].toExternalForm());
			try {
				InputStream in = imageUrls[0].openStream();
				return BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage(), e);
				Toast.makeText(MainActivity.this, "An error occurred while downloading avatar.", Toast.LENGTH_SHORT).show();
				return null;
			}
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
		}
	}
}
